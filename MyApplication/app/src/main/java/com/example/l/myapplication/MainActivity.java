package com.example.l.myapplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyStore;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

public class MainActivity extends Activity {

    ListView list;
    /*String[] url = new String[]{
            "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400\u0026photoreference=CnRnAAAAesUQ_9WjnCoWgRyfKnNfwgFTMeRnGDsQcII5NQ_KMlyMpl4GKjYgCpFXBPLEXczyyRMnD36hnH5qBFtUQxjEtM9xwkSxp8ViiLE7svXb62tj-EIp-i-95sPlTe14UbGoSNusr7MPBvPJtSb-qMMNjRIQhV9vo2gVySwGQiXxso2-RxoUmSHbEHI-820zUxZJqNCW1S6DJj8\u0026key=AIzaSyDUPWbqqaAynMr8ndQurW8IhlwWfXetTYk",
            "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400\u0026photoreference=CnRnAAAA2vp3qtPJM3gGKihltjuHzlW9sk9DSKa_1nEtvHceCCnmIXYNk8VmhKjf_xLkrB4bIE3RyOpFpyEZL1IWuqkoDIOiqMVmlfWvpGYejDNhzVelxs1F-qIeV1LQQxGWZtWYKv0bG8gse_XY6M_q8IJJDBIQdzxjpYMfjknMknzH4NdodhoUvaMy7_1LQ7Huv6LrZvd_eJ65z0c\u0026key=AIzaSyDUPWbqqaAynMr8ndQurW8IhlwWfXetTYk"};
    String[] names = new String[]{"宮都拉義式主題餐廳", "陶板屋- 台北光復南店"};
    String[] lists = new String[]{"大安區忠孝東路四段170巷17弄5號", "台北市大安區忠孝東路四段"};
    String[] rating = new String[]{"2.5", "4"};
    */
    HashMap<Integer, String> icon = new HashMap<Integer, String>();
    HashMap<Integer, String> names = new HashMap<Integer, String>();
    HashMap<Integer, String> address = new HashMap<Integer, String>();
    HashMap<Integer, String> rating = new HashMap<Integer, String>();

    Adapter adapter = null;
    DownloadWebPicture loadPic;
    Handler mHandler;
    final HashMap<String, Bitmap> picmap = new HashMap<String, Bitmap>();

    int objCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = (ListView) findViewById(R.id.listView1);
        adapter = new Adapter(this);


        loadPic = new DownloadWebPicture();

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Log.e("test", msg.toString());
                picmap.put(icon.get(msg.what), loadPic.getImg());
                list.setAdapter(adapter);

                super.handleMessage(msg);
            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    String jsonurl = new String(
                            "http://140.135.10.109/test.html");
                    try {
                        JSONObject json = getJson(jsonurl);
                        String data = json.getString("results");
                        JSONArray jsonarr = new JSONArray(data);
                        Log.e("test", "jsonArr" + json.toString());

                        objCount = jsonarr.length();

                        for (int i = 0; i < jsonarr.length(); i++) {
                            Log.e("test", "icon" + jsonarr.getJSONObject(i).optString("icon"));
                            icon.put(i, jsonarr.getJSONObject(i).optString("icon"));
                            Log.e("test", "name" + jsonarr.getJSONObject(i).optString("name"));
                            names.put(i, jsonarr.getJSONObject(i).optString("name"));
                            Log.e("test", "address" + jsonarr.getJSONObject(i).optString("address"));
                            address.put(i, jsonarr.getJSONObject(i).optString("address"));
                            Log.e("test", "rating" + jsonarr.getJSONObject(i).optString("rating"));
                            rating.put(i, jsonarr.getJSONObject(i).optString("rating"));
                        }


                        for (int i = 0; i < jsonarr.length(); i++) {
                            loadPic.handleWebPic(i, mHandler);
                        }


                        list.setAdapter(adapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();


        //Log.e("test",String.valueOf(icon.size()));


    }

    public static JSONObject getJson(String url) {

        InputStream is = null;
        String result = "";
        JSONObject jsonObj = null;

        try {
            //URL jsonurl = new URL(url);
            //HttpsURLConnection urlConnection = (HttpsURLConnection)jsonurl.openConnection();
            //urlConnection.setRequestProperty("Authorization", "Basic "+"7H30jZsPDdT5kbtRoMe3k4E9jMVYuCf61gUVcDxA:jicHgAw2mYUqePHHRChLp2TMouh7I1N4Ood8lNuiZ");
            //is = urlConnection.getInputStream();

            URL jsonurl = new URL(url);
            URLConnection urlConnection = jsonurl.openConnection();
            is = urlConnection.getInputStream();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf8"), 9999999);
            StringBuilder sb = new StringBuilder();
            String line = null;

            while ((line = reader.readLine()) != null) {
                Log.e("test", "line" + line);
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            jsonObj = new JSONObject(result);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObj;
    }


    public class Adapter extends BaseAdapter {
        private LayoutInflater inflater;

        public Adapter(Context c) {
            inflater = LayoutInflater.from(c);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return 200;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return names.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            convertView = inflater.inflate(R.layout.listlayout, null);

            final ImageView img = (ImageView) convertView.findViewById(R.id.image);
            TextView name = (TextView) convertView.findViewById(R.id.tittle);
            TextView list = (TextView) convertView.findViewById(R.id.address);
            RatingBar ratingbar = (RatingBar) convertView.findViewById(R.id.ratingBar1);

            img.setImageBitmap(picmap.get(icon.get(position % objCount)));

            name.setText(names.get(position % objCount));
            list.setText(address.get(position % objCount));
            ratingbar.setRating(Float.parseFloat(rating.get(position % objCount)));


            return convertView;
        }

    }

    public class DownloadWebPicture {
        Bitmap bmp;

        public synchronized Bitmap getUrlPic(String url) {
            Bitmap webImg = null;

            try {
                URL imgUrl = new URL(url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) imgUrl.openConnection();
                httpURLConnection.connect();
                InputStream inputStream = httpURLConnection.getInputStream();
                int length = (int) httpURLConnection.getContentLength();
                int tmpLength = 512;
                int readLen = 0, desPos = 0;
                byte[] img = new byte[length];
                byte[] tmp = new byte[tmpLength];
                if (length != -1) {
                    while ((readLen = inputStream.read(tmp)) > 0) {
                        System.arraycopy(tmp, 0, img, desPos, readLen);
                        desPos += readLen;
                    }
                    webImg = BitmapFactory.decodeByteArray(img, 0, img.length);
                    if (desPos != length) {
                        throw new IOException("Only read" + desPos + "bytes");
                    }
                }
                httpURLConnection.disconnect();
            } catch (IOException e) {
                Log.e("IOException", e.toString());
            }
            return webImg;
        }

        public void handleWebPic(final int key, final Handler handler) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    bmp = getUrlPic(icon.get(key));
                    Message msg = new Message();
                    msg.what = key;
                    handler.sendMessage(msg);
                }
            }).start();
        }

        public Bitmap getImg() {
            return bmp;
        }
    }
}