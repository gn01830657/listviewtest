package com.example.test;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

public class MainActivity extends Activity {

	ListView list;
	String[] url = new String[] {
			"https://maps.googleapis.com/maps/api/place/photo?maxwidth=400\u0026photoreference=CnRnAAAAesUQ_9WjnCoWgRyfKnNfwgFTMeRnGDsQcII5NQ_KMlyMpl4GKjYgCpFXBPLEXczyyRMnD36hnH5qBFtUQxjEtM9xwkSxp8ViiLE7svXb62tj-EIp-i-95sPlTe14UbGoSNusr7MPBvPJtSb-qMMNjRIQhV9vo2gVySwGQiXxso2-RxoUmSHbEHI-820zUxZJqNCW1S6DJj8\u0026key=AIzaSyDUPWbqqaAynMr8ndQurW8IhlwWfXetTYk",
			"https://maps.googleapis.com/maps/api/place/photo?maxwidth=400\u0026photoreference=CnRnAAAA2vp3qtPJM3gGKihltjuHzlW9sk9DSKa_1nEtvHceCCnmIXYNk8VmhKjf_xLkrB4bIE3RyOpFpyEZL1IWuqkoDIOiqMVmlfWvpGYejDNhzVelxs1F-qIeV1LQQxGWZtWYKv0bG8gse_XY6M_q8IJJDBIQdzxjpYMfjknMknzH4NdodhoUvaMy7_1LQ7Huv6LrZvd_eJ65z0c\u0026key=AIzaSyDUPWbqqaAynMr8ndQurW8IhlwWfXetTYk" };
	String[] names = new String[] { "宮都拉義式主題餐廳", "陶板屋- 台北光復南店" };
	String[] lists = new String[] { "大安區忠孝東路四段170巷17弄5號", "台北市大安區忠孝東路四段" };
	String[] rating = new String[]{"2.5","4"};

	Adapter adapter = null;
	DownloadWebPicture loadPic;
	Handler mHandler;
	final HashMap<String, Bitmap> picmap = new HashMap<String, Bitmap>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
/*
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					String jsonurl = new String(
							"https://api.parse.com/1/classes/store/");
					try {
						JSONArray jsonarr = getJson(jsonurl);
						
						url[1] = jsonarr.getJSONObject(0).getString("icon");
						names[1] = jsonarr.getJSONObject(0).getString("name");
						lists[1] = jsonarr.getJSONObject(0).getString("address");

						url[0] = jsonarr.getJSONObject(1).getString("icon");
						names[0] = jsonarr.getJSONObject(1).getString("name");
						lists[0] = jsonarr.getJSONObject(1).getString("address");
					} catch (Exception e) {
						e.printStackTrace();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
*/
		loadPic = new DownloadWebPicture();

		mHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {

				picmap.put(url[msg.what], loadPic.getImg());
				list.setAdapter(adapter);

				super.handleMessage(msg);
			}
		};
		for (int i = 0; i < url.length; i++) {
			loadPic.handleWebPic(i, mHandler);
		}

		list = (ListView) findViewById(R.id.listView1);
		adapter = new Adapter(this);

		list.setAdapter(adapter);

	}
/*
	public static JSONArray getJson(String url){
		 
		InputStream is = null;
		String result = "";
		//若線上資料為陣列，則使用JSONArray
		JSONArray jsonArray = null;
		//若線上資料為單筆資料，則使用JSONObject
		//JSONObject jsonObj = null;
 
		// 透過HTTP連線取得回應
		try {
			HttpClient httpclient = new DefaultHttpClient(); // for port 80 requests!
			HttpPost httppost = new HttpPost(url);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		} catch(Exception e) {
			e.printStackTrace();
		}
 
		// 讀取回應
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf8"),9999999);
			//99999為傳流大小，若資料很大，可自行調整
			StringBuilder sb = new StringBuilder();
			String line = null;
 
			while ((line = reader.readLine()) != null) {
				//逐行取得資料

				Log.e("test", line);
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch(Exception e) {
			e.printStackTrace();
		}
		//轉換文字為JSONArray
		try {

			Log.e("test", result);
			 jsonArray = new JSONArray(result);
		} catch(JSONException e) {
			e.printStackTrace();
		}
 
		return jsonArray;
 
		//轉換文字為JSONObject
		/*
		try {
			 jsonObj = new JSONObject(result);
		} catch(JSONException e) {
			e.printStackTrace();
		}
 
		return jsonObj;
		*/
 
	//}
	


	public class Adapter extends BaseAdapter {
		private LayoutInflater inflater;

		public Adapter(Context c) {
			inflater = LayoutInflater.from(c);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 200;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return names[position];
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			convertView = inflater.inflate(R.layout.listlayout, null);

			final ImageView img = (ImageView) convertView.findViewById(R.id.image);
			TextView name = (TextView) convertView.findViewById(R.id.tittle);
			TextView list = (TextView) convertView.findViewById(R.id.address);
			RatingBar ratingbar = (RatingBar)convertView.findViewById(R.id.ratingBar1);

			img.setImageBitmap(picmap.get(url[position%2]));

			name.setText(names[position%2]);
			list.setText(lists[position%2]);
			ratingbar.setRating(Float.parseFloat(rating[position%2]));
			

			return convertView;
		}

	}

	public class DownloadWebPicture {
		Bitmap bmp;

		public synchronized Bitmap getUrlPic(String url) {
			Bitmap webImg = null;

			try {
				URL imgUrl = new URL(url);
				HttpURLConnection httpURLConnection = (HttpURLConnection) imgUrl.openConnection();
				httpURLConnection.connect();
				InputStream inputStream = httpURLConnection.getInputStream();
				int length = (int) httpURLConnection.getContentLength();
				int tmpLength = 512;
				int readLen = 0, desPos = 0;
				byte[] img = new byte[length];
				byte[] tmp = new byte[tmpLength];
				if (length != -1) {
					while ((readLen = inputStream.read(tmp)) > 0) {
						System.arraycopy(tmp, 0, img, desPos, readLen);
						desPos += readLen;
					}
					webImg = BitmapFactory.decodeByteArray(img, 0, img.length);
					if (desPos != length) {
						throw new IOException("Only read" + desPos + "bytes");
					}
				}
				httpURLConnection.disconnect();
			} catch (IOException e) {
				Log.e("IOException", e.toString());
			}
			return webImg;
		}

		public void handleWebPic(final int key, final Handler handler) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					bmp = getUrlPic(url[key]);
					Message msg = new Message();
					msg.what = key;
					handler.sendMessage(msg);
				}
			}).start();
		}

		public Bitmap getImg() {
			return bmp;
		}
	}
}